# BA Thesis supplementary material

This repository contains the code for the experiment and analysis aswell as the corresponding data from my bachelors thesis with the titel *Implicit Gender Bias in the German Federal Elections (2021) – Is there an Effect of the Masculine Generic Form?*.
The collected data is anonymized and the participants gave informed consent for it being published.
