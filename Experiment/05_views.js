// In this file you can instantiate your views
// We here first instantiate wrapping views, then the trial views


/** Wrapping views below

* Obligatory properties

    * trials: int - the number of trials this view will appear
    * name: string

*Optional properties
    * buttonText: string - the text on the button (default: 'next')
    * text: string - the text to be displayed in this view
    * title: string - the title of this view

    * More about the properties and functions of the wrapping views - https://magpie-ea.github.io/magpie-docs/01_designing_experiments/01_template_views/#wrapping-views

*/

// Every experiment should start with an intro view. Here you can welcome your participants and tell them what the experiment is about
const welcome = magpieViews.view_generator("intro", {
    trials: 1,
    name: 'welcome',
    title : 'Willkommen zu einem Experiment zur Bundestagwahl 2021',
    // If you use JavaScripts Template String `I am a Template String`, you can use HTML <></> and javascript ${} inside
    text: `Mein Name ist Erik Nickel und ich f&uumlhre im Rahmen meiner Bachelorarbeit dieses Experiment durch.  Bei aufkommenden Fragen wenden Sie sich gerne an mich unter <a href=mailto:ernickel@uos.de>ernickel@uos.de<a>. </br>Vielen Dank, dass Sie sich entschieden haben an meinem Experiment teilzunehmen. Wenn Sie Versuchpersonenstunden erhalten wollen, k&oumlnnen Sie am Ende erfahren, wie Sie diese erhalten. Bitte stellen Sie sicher, dass Sie f&uumlr das Experiment einen Computer oder Laptop mit eigenst&aumlndiger Tastatur benutzen, damit alle Bestandteile wie vorgesehen funktionieren. </br>
 Dieses Experiment setzt sich mit der Wahrnehmung der Bev&oumllkerung zu der anstehenden Bundestagswahl am 26. September 2021 auseinander. Dieses Experiment ist mehrteilig, vor jeder neuen Aufgabe erhalten Sie detaillierte Angaben. Aufgrund des Designs ist es nicht m&oumlglich zu einer vorherigen Aufgabe zur&uumlckzukehren.
</br>

`,
    buttonText: 'Weiter'
});

const consentForm = magpieViews.view_generator("instructions",{
    trials: 1,
    name: "consentForm",
    title: "Teilnehmendeninformation und Einverst&aumlndniserkl&aumlrung",
    buttonText: "Weiter",
    text: `
</br><strong   >Versuchsablauf</strong></br>
 Das Experiment wird insgesamt etwa 10-30 Minuten in Anspruch nehmen. 
 Das Experiment besteht aus vier Aufgaben, welche Sie nacheinander durchf&uumlhren.  Sie k&oumlnnen gerne vor jeder Aufgabe eine kurze Pause einlegen, jedoch m&oumlchte ich Sie bitten, sich w&aumlhrend der gesamten Dauer des Experiments nicht ablenken zu lassen und nach M&oumlglichkeit nicht von ihrem Ger&aumlt aufzustehen. 
 </br><strong   >Was wird gemessen?</strong></br>
 Gemessen werden alle von Ihnen get&aumltigten Eingaben sowie die Zeit, die Sie sich f&uumlr die einzelnen Aufgabenteile nehmen. 
 </br><strong   >Sicherheitshinweis</strong></br>
 Sie werden gebeten, bei k&oumlrperlichem oder psychischem Unwohlsein unverz&uumlglich den Versuch abzubrechen. Hierdurch entstehen f&uumlr Sie keine Nachteile. 
 </br><strong   >Freiwillige Teilnahme, Umgang mit Ihren Daten</strong></br>
 Sie k&oumlnnen Ihre Teilnahme jederzeit und ohne Angabe von Gr&uumlnden abbrechen. Die Daten werden erst erhoben, nachdem Sie die Studie komplett abgeschlossen haben.
Alle Daten, die in diesem Experiment erhoben werden, werden v&oumlllig anonymisert behandelt. Ihre Daten werden nur zu Forschungszwecken gespeichert und verwertet. Alle Daten werden gem&aumlß den Empfehlungen der Deutschen Forschungsgemeinschaft (DFG) f&uumlr mindestens 10 Jahre nach der Datenerhebung aufbewahrt. Die anonymen Daten dieser Studie werden zudem gegebenfalls als offene Daten zug&aumlnglich gemacht werden.  
</br>
</br><strong   >Einverst&aumlndnis</strong></br>
Bitte best&aumltigen Sie durch das Setzen des Hackens die folgende Aussage: “Hiermit best&aumltige ich, dass ich &uumlber Zwecke und Ablauf der Studie aufgekl&aumlrt und informiert worden bin. Ich habe diese Erkl&aumlrung gelesen und verstanden. Ich stimme jedem der Punkte zu. Ich erm&aumlchtige hiermit die von mir in dieser Untersuchung erworbenen Daten zu wissenschaftlichen Zwecken zu analysieren und in wissenschaftlichen Arbeiten anonymisiert zu ver&oumlffentlichen. Ich wurde &uumlber meine Rechte als Versuchsperson informiert und erkl&aumlre mich zu der freiwilligen Teilnahme an dieser Studie bereit.”
</br>
</br>
Danach k&oumlnnen Sie durch das Klicken auf "Weiter" mit dem Experiment beginnen. 


    
    `

},{
answer_container_generator: function (config, CT) {
    return `<div class='magpie-view-answer-container'>
             <div>
            <input type="checkbox" id="consent" name="consent" >
            <label id="consentbox" for="consent">Ich bin einverstanden.</label>
             </div>
            <button id="next" class='magpie-view-button magpie-nodisplay' >${config.button}</button>
           
            </div>`;

},

handle_response_function: function(config, CT, magpie, answer_container_generator, startingTime) {
   
    $(".magpie-view").append(answer_container_generator(config, CT));
    let counterConsent = 1;
    let gaveConsent = false;
    $("#consent").on("change", function () {
        counterConsent = counterConsent +1;
        if (counterConsent % 2 == 0){
            gaveConsent = true;
            $("#next").removeClass("magpie-nodisplay");
        } else {
            gaveConsent = false;
            $("#next").addClass("magpie-nodisplay");
        }

    });
     
  

   


  

    // moves to the next view
    $("#next").on("click", function() {
        const RT = Date.now() - startingTime; // measure RT before anything else
                let trial_data = {
                    trial_name: config.name,
                    trial_number: CT + 1,
                    consent: gaveConsent,
                    RT: RT
                };

                trial_data = magpieUtils.view.save_config_trial_data(config.data[CT], trial_data);

                magpie.trial_data.push(trial_data);

        magpie.findNextView();
    });
}
}
)

const instructionsMC = magpieViews.view_generator("instructions", {
    trials: 1,
    name: 'instructionsMC',
    title: 'Medienkonsum',
    buttonText: 'Weiter',
    text: 'Bitte w&aumlhlen Sie in der n&aumlchsten Aufgabe die Option aus, die Ihren Medienkonsum bez&uumlglich des Themas am besten widerspiegelt.'
});
// For most tasks, you need instructions views
const instructionsTC = magpieViews.view_generator("instructions", {
  trials: 1,
  name: 'instructionsTC',
  title: 'Schreibaufgabe',
  text: `In dieser Aufgabe geht es darum, S&aumltze, die begonnen wurden, fortzusetzen. Es folgen vier Wiederholungen dieser Aufgabe. Dabei wird es jeweils eine nicht variable Einf&uumlhrung geben, gefolgt von jeweils vier unterschiedlichen nicht beendeten S&aumltzen. Ihre Aufgabe ist es, diese S&aumltze kurz fortzuf&uumlhren, auf eine Art, dass der produzierte Text so als Teil eines Medienberichtes zu der anstehenden Bundestagswahl ver&oumlffentlicht werden k&oumlnnte. Bitte schreiben Sie einige Worte, da das "Weiter" Feld erst erscheint, nachdem Sie mindestens zehn Zeichen eingegeben haben.`,
  buttonText: 'Weiter'
});
const instructionsSPR = magpieViews.view_generator("instructions", {
    trials: 1,
    name: 'instructionsSPR',
    title: 'Leseaufgabe',
    // text: "Im folgenden werden Sie neun kurze Texte zu der anstehenden Bundestagwahl sehen.Ihre Aufgabe ist es die Texte aufmerksam zu Lesen und die danach folgende Frage zu beantworten.Die Text sind zu n&aumlchste verdeckt und sie k&oumlnnen individuell, durch das Dr&uumlcken der Leertaste, die W&oumlrter erscheinen lassen.",
    text: `In der folgenden Aufgabe werden Sie jeweils einen Einf&uumlhrungssatz sehen und darunter sich &aumlndernde Weiterf&uumlhrungen des Einf&uumlhrungssatzes. Der weiterf&uumlhrende Satz ist verdeckt und durch das Dr&uumlcken der Leertaste auf Ihrer Tastatur k&oumlnnen Sie das n&aumlchste Wort sichtbar machen, dabei verschwindet das jeweils vorherige Wort. Ich m&oumlchten Sie bitten, die S&aumltze so schnell und genau wie m&oumlglich zu lesen. Die Aufgabe besteht aus neun Runden. Bitte nehmen Sie sich nach jeder Runde so viel Zeit, wie Sie brauchen um die n&aumlchste Runde schnell und konzentriert zu bearbeiten. Es wird nach jeder Leserunde eine Verst&aumlndnisfrage zu dem vorhergehenden Inhalt geben, die Sie bitte m&oumlglichst richtig mit "Wahr", so stand es im Text, oder "Falsch", diese Information kam im Text nicht vor, beantworten. Bevor das eigentliche Experiment beginnt, wird es zwei Einf&uumlhrungsrunden geben, damit Sie sich mit der Aufgabe vertraut machen k&oumlnnen. Danach beginnen direkt die eigentlichen S&aumltze. Am blauen Balken in der linken oberen Ecke k&oumlnnen Sie Ihren Fortschritt erkennen.`,
    buttonText: 'Weiter'
});
const instructionsBE = magpieViews.view_generator("instructions", {
    trials: 1,
    name: 'instructionsBE',
    title: 'Einsch&aumltzungsaufgabe',
    text: `In der folgenden Aufgabe sollen Sie f&uumlr drei Personen, die &oumlffentlich f&uumlr ihre Kanzlerschaftskandidatur werben, angeben, wie Sie f&uumlr jede der Personen die Wahrscheinlichkeit einsch&aumltzen, dass sie tats&aumlchlich die Kanzlerschaft &uumlbernimmt. Dabei ist es wichtig zu beachten, dass es nicht darum geht, wen Sie f&uumlr am f&aumlhigsten halten oder am liebsten in diesem Amt h&aumltten, sondern allein, wer es tats&aumlchlich wird.

Um diese Einsch&aumltzung vorzunehmen, werden Sie unter jedem der Namen jeweils einen Schieber sehen, wobei die linke Seite f&uumlr "nicht wahrscheinlich" steht und die rechte Seite f&uumlr "sehr wahrscheinlich". Damit Sie weiter kommen m&uumlssen Sie jeden der Regler mindesten einmal angeklickt haben und dann auf "Fertig" klicken. Wenn Sie jeden der Regler einmal angeklickt haben, erscheint "Weiter".`,
    buttonText: 'Weiter'
});
const debriefing = magpieViews.view_generator('instructions', {
    trials: 1,
    name: "debriefing",
    title: "Zur Zielsetzung dieser Studie",
    text: `Dieses Experiment benutzt implizite Methoden, die anf&aumlllig f&uumlr Voreingenommenheit sind. Um m&oumlgliche Einfl&uumlsse durch Voreingenommenheit zu vermeiden, wurde Ihnen am Anfange des Experimentes nur ein &Uumlberblick gegeben, der hier nun verfeinert wird.  Der Fokus dieser Studie liegt auf dem Zusammenspiel von Stereotypen und Erwartungshaltungen mit besonderem Augenmerk auf das generische Maskulinum. Als generische Maskulinum wird die Benutzung einer grammatikalisch maskulinen Form bezeichnet, welche nicht das Geschlecht einer Person bezeichent, sondern eine Person oder Gruppe f&uumlr welche das Geschlecht nicht bekannt ist oder bei der es keine Rolle spielt.  Hierzu werden Ihr Gebrauch von Nomen und m&aumlnnlichen, weiblichen und gemischten Pronomen in der Schreibaufgabe, sowie Ihre Wahrnehmung von Nomen und Pronomen in der Leseaufgabe, in Form der Lesegeschwindigkeit, ausgewertet.
</br></br>Wenn Sie an der Universit&aumlt Osnabr&uumlck Psychologie oder Cognitive Science studieren k&oumlnnen Sie 0,5 Versuchspersonenstunden bekommen. Bitte schicken Sie daf&uumlr eine Mail mit dem Betreff "VP: Bundestagswahl_2021-GM" an meine Mailadresse: <a href=mailto:ernickel@uos.de>ernickel@uos.de<a> und schreiben Sie mir Ihren vollst&aumlndigen Namen sowie Ihre Matrikelnummer, damit ich die Daten an das Pr&uumlfungsamt weiter geben kann.</br></br>
Um das Experiment erfolgreich abzuschlie&szligen klicken Sie jetzt bitte auf "Weiter".`,
    buttonText: "Weiter"
}
);


// In the post test questionnaire you can ask your participants addtional questions
const post_test = magpieViews.view_generator("post_test", {
  trials: 1,
  name: 'post_test',
  title: 'Weitere Informationen',
  text: 'Es hilft bei der Auswertung der Studie, wenn sie diesen Fragebogen ausf&uumlllen.',
  // You can change much of what appears here, e.g., to present it in a different language, as follows:
  buttonText: 'Weiter',
  age_question: 'Alter',
  gender_question: 'Geschlecht',
  gender_male: 'm&aumlnnlich',
  gender_female: 'weiblich',
  gender_other: 'divers',
  edu_question: 'H&oumlchster Bildungsabschluss',
  edu_graduated_high_school: 'Schulabschluss',
  edu_graduated_college: 'Hochschulabschluss',
  edu_higher_degree: 'H&oumlherer Universit&aumlrer Abschluss',

  languages_question: 'Muttersprache',
  languages_more: '(in der Regel die Sprache, die Sie als Kind zu Hause gesprochen haben)',
  comments_question: 'Weitere Kommentare'
},
{answer_container_generator: function(config, CT) {
    const quest = magpieUtils.view.fill_defaults_post_test(config);
    return `<form>
                <p class='magpie-view-text'>
                    <label for="age">${quest.age.title}:</label>
                    <input type="number" name="age" min="18" max="110" id="age" />
                </p>
                <p class='magpie-view-text'>
                    <label for="gender">${quest.gender.title}:</label>
                    <select id="gender" name="gender">
                        <option></option>
                        <option value="${quest.gender.male}">${quest.gender.male}</option>
                        <option value="${quest.gender.female}">${quest.gender.female}</option>
                        <option value="${quest.gender.other}">${quest.gender.other}</option>
                    </select>
                </p>
                <p class='magpie-view-text'>
                    <label for="education">${quest.edu.title}:</label>
                    <select id="education" name="education">
                        <option></option>
                        <option value="${quest.edu.graduated_high_school}">${quest.edu.graduated_high_school}</option>
                        <option value="${quest.edu.graduated_college}">${quest.edu.graduated_college}</option>
                        <option value="${quest.edu.higher_degree}">${quest.edu.higher_degree}</option>

                    </select>
                </p>
                <p class='magpie-view-text'>
                    <label for="occupation">Besch&aumlftigung:</label>
                    <select id="occupation" name="occupation">
                        <option></option>
                        <option value="Studierend">Studierend</option>
                        <option value="Berufst&aumltig">Berufst&aumltig</option>
                        <option value="Schulische oder Berufsausbildung">Ausbildung</option>
                        <option value="Keine dieser Optionen">Keine dieser Optionen</option>                        
                    </select>
                </p>
                <p class='magpie-view-text'>
                    <label for="languages" name="languages">${quest.langs.title}:<br /><span>${quest.langs.text}</</span></label>
                    <input type="text" id="languages"/>
                </p>
                <p class="magpie-view-text">
                    <label for="comments">${quest.comments.title}</label>
                    <textarea name="comments" id="comments" rows="6" cols="40"></textarea>
                </p>
                <button id="next" class='magpie-view-button'>${config.button}</button>
        </form>`
}},
{
handle_response_function: function(config, CT, magpie, answer_container_generator, startingTime) {
    $(".magpie-view").append(answer_container_generator(config, CT));

    $("#next").on("click", function(e) {
        // prevents the form from submitting
        e.preventDefault();

        // records the post test info
        magpie.global_data.age = $("#age").val();
        magpie.global_data.gender = $("#gender").val();
        magpie.global_data.education = $("#education").val();
        magpie.global_data.languages = $("#languages").val();
        
        magpie.global_data.comments = $("#comments")
        .val()
        .trim();
        magpie.global_data.endTime = Date.now();
        magpie.global_data.timeSpent =
            (magpie.global_data.endTime -
                magpie.global_data.startTime) /
            60000;

        // moves to the next view
        magpie.findNextView();
    });
}});

// The 'thanks' view is crucial; never delete it; it submits the results!
const thanks = magpieViews.view_generator("thanks", {
  trials: 1,
  name: 'thanks',
    title: `Vielen Dank f&uumlr die Teilnahme an diesem Experiment.
    </br>
  Sie k&oumlnnen diesen Tab nun schlie&szligen.`,
 // prolificConfirmText: 'Bitte den Knopf dr&uumlcken'
});

/**
 *  trial (magpie's Trial Type Views) below

* Obligatory properties

    - trials: int - the number of trials this view will appear
    - name: string - the name of the view type as it shall be known to _magpie (e.g. for use with a progress bar)
            and the name of the trial as you want it to appear in the submitted data
    - data: array - an array of trial objects

* Optional properties

    - pause: number (in ms) - blank screen before the fixation point or stimulus show
    - fix_duration: number (in ms) - blank screen with fixation point in the middle
    - stim_duration: number (in ms) - for how long to have the stimulus on the screen
      More about trial life cycle - https://magpie-ea.github.io/magpie-docs/01_designing_experiments/04_lifecycles_hooks/

    - hook: object - option to hook and add custom functions to the view
      More about hooks - https://magpie-ea.github.io/magpie-docs/01_designing_experiments/04_lifecycles_hooks/

* All about the properties of trial views
* https://magpie-ea.github.io/magpie-docs/01_designing_experiments/01_template_views/#trial-views
*/

/**
// Here, we initialize a normal forced_choice view
const forced_choice_2A = magpieViews.view_generator("forced_choice", {
  // This will use all trials specified in `data`, you can user a smaller value (for testing), but not a larger value
  trials: trial_info.forced_choice.length,
  // name should be identical to the variable name
  name: 'forced_choice_2A',
  data: trial_info.forced_choice,
  // you can add custom functions at different stages through a view's life cycle
  // hook: {
  //     after_response_enabled: check_response
  // }
});
*/


const self_paced = magpieViews.view_generator("self_paced_reading", {
    trials: trial_info.spr_trials.length,
    name: 'self_paced',
    data: trial_info.spr_trials
},
    {
        answer_container_generator: function (config, CT) {
            return `<div class='magpie-view-answer-container'>

                    <p id="sprCQ" class='magpie-view-question'>${config.data[CT].question}</p>
                    <label id="sprCQB1" for='o1' class='magpie-response-buttons'>${config.data[CT].option1}</label>
                    <input type='radio' name='answers' class='magpie-nodisplay' id='o1' value=${config.data[CT].option1} />
                    <input type='radio' name='answers' class='magpie-nodisplay' id='o2' value=${config.data[CT].option2} />
                    <label id="sprCQB2" for='o2' class='magpie-response-buttons'>${config.data[CT].option2}</label>

                    <p id='sprRQ' class='magpie-view-question magpie-nodisplay'>${config.data[CT].questionR}</p>
                    <strong id='sprRQL' class='magpie-response-rating-option magpie-view-text magpie-nodisplay'>${config.data[CT].optionLeft}</strong>
                    <label id="b1" for="1" class='magpie-response-rating magpie-nodisplay'>1</label>
                    <input type="radio" name="answer" id="1" value="1" />
                    <label id="b2" for="2" class='magpie-response-rating magpie-nodisplay'>2</label>
                    <input type="radio" name="answer" id="2" value="2" />
                    <label id="b3" for="3" class='magpie-response-rating magpie-nodisplay'>3</label>
                    <input type="radio" name="answer" id="3" value="3" />
                    <label id="b4" for="4" class='magpie-response-rating magpie-nodisplay'>4</label>
                    <input type="radio" name="answer" id="4" value="4" />
                    <label id="b5" for="5" class='magpie-response-rating magpie-nodisplay'>5</label>
                    <input type="radio" name="answer" id="5" value="5" />
                    <label id="b6" for="6" class='magpie-response-rating magpie-nodisplay'>6</label>
                    <input type="radio" name="answer" id="6" value="6" />
                    <label id="b7" for="7" class='magpie-response-rating magpie-nodisplay'>7</label>
                    <input type="radio" name="answer" id="7" value="7" />
                    <strong id='sprRQR' class='magpie-response-rating-option magpie-view-text magpie-nodisplay'>${config.data[CT].optionRight}</strong>

                </div>`
        },
        handle_response_function: function (config, CT, magpie, answer_container_generator, startingTime) {

            const sentenceList = config.data[CT].sentence.trim().split(" | ");
            let spaceCounter = 0;
            let wordList;
            let readingTimes = [];
            // wordPos "next" or "same", if "next" words appear next to each other, if "same" all words appear at the same place
            // default: "next"
            let wordPos = config.data[CT].wordPos === undefined ? "next" : config.data[CT].wordPos;
            let showNeighbor = wordPos === "next";
            // underline "words", "sentence" or "none", if "words" every word gets underlined, if "sentence" the sentence gets
            // underlined, if "none" there is no underline
            // default: "words"
            let underline = config.data[CT].underline === undefined ? "words" : config.data[CT].underline;
            let not_underline = underline === "none";
            let one_line = underline === "sentence";

            // shows the sentence word by word on SPACE press
            const handle_key_press = function (e) {

                if (e.which === 32 && spaceCounter < sentenceList.length) {
                    if (showNeighbor) {
                        wordList[spaceCounter].classList.remove("spr-word-hidden");
                    } else {
                        $(".magpie-spr-sentence").html(`<span class='spr-word'>${sentenceList[spaceCounter]}</span>`);
                        if (not_underline) {
                            $('.magpie-spr-sentence .spr-word').addClass('no-line');
                        }
                    }

                    if (spaceCounter === 0) {
                        $(".magpie-help-text").addClass("magpie-invisible");
                    }

                    if (spaceCounter > 0 && showNeighbor) {
                        wordList[spaceCounter - 1].classList.add("spr-word-hidden");
                    }

                    readingTimes.push(Date.now());
                    spaceCounter++;
                } else if (e.which === 32 && spaceCounter === sentenceList.length) {
                    if (showNeighbor) {
                        wordList[spaceCounter - 1].classList.add("spr-word-hidden");
                    } else {
                        $(".magpie-spr-sentence").html("");
                    }

                    $(".magpie-view").append(answer_container_generator(config, CT));
                    $("input[name=answers]").on("change", function () {



                        answerQC = $("input[name=answers]:checked").val();

                        $("#sprCQ").addClass("magpie-nodisplay");
                        $("#sprCQB1").addClass("magpie-nodisplay");
                        $("#sprCQB2").addClass("magpie-nodisplay");
                        $("#sprRQ").removeClass("magpie-nodisplay");
                        $("#sprRQL").removeClass("magpie-nodisplay");
                        $("#sprRQR").removeClass("magpie-nodisplay");
                        $("#b1").removeClass("magpie-nodisplay");
                        $("#b2").removeClass("magpie-nodisplay");
                        $("#b3").removeClass("magpie-nodisplay");
                        $("#b4").removeClass("magpie-nodisplay");
                        $("#b5").removeClass("magpie-nodisplay");
                        $("#b6").removeClass("magpie-nodisplay");
                        $("#b7").removeClass("magpie-nodisplay");
                    });
                    
                    $("input[name=answer]").on("change", function () {
                        const RT = Date.now() - startingTime;
                        let reactionTimes = readingTimes
                            .reduce((result, current, idx) => {
                                return result.concat(
                                    readingTimes[idx + 1] - readingTimes[idx]
                                );
                            }, [])
                            .filter((item) => isNaN(item) === false);
                        let trial_data = {
                            trial_name: config.name,
                            trial_number: CT + 1,
                            response: $("input[name=answer]:checked").val(),
                            reaction_times: reactionTimes,
                            time_spent: RT,
                            sprCQ_expected: config.data[CT].sprCQ_expected,
                            sprQC: answerQC,
                            sprQCtrue: config.data[CT].sprCQ_expected == answerQC
                        };

                        trial_data = magpieUtils.view.save_config_trial_data(config.data[CT], trial_data);

                        magpie.trial_data.push(trial_data);
                        magpie.findNextView();
                    });
                    readingTimes.push(Date.now());
                    spaceCounter++;
                }
            };
            // shows the help text
            $(".magpie-help-text").removeClass("magpie-nodisplay");

            if (showNeighbor) {
                // creates the sentence
                sentenceList.map((word) => {
                    $(".magpie-spr-sentence").append(
                        `<span class='spr-word spr-word-hidden'>${word}</span>`
                    );
                });

                // creates an array of spr word elements
                wordList = $(".spr-word").toArray();
            }

            if (not_underline) {
                $('.magpie-spr-sentence .spr-word').addClass('no-line');
            }
            if (one_line) {
                $('.magpie-spr-sentence .spr-word').addClass('one-line');
            }

            // attaches an eventListener to the body for space
            $("body").on("keydown", handle_key_press);

        }
    });

// this is a custom implementation of the slider view which enables multible slider on one screen
const multi_slider_rating = magpieViews.view_generator("slider_rating", {
    trials: trial_info.multi_slider_rating_trials.length,
    name: 'multi_slider_rating',
    data: trial_info.multi_slider_rating_trials
},
    {
        answer_container_generator: function (config, CT) {
            return `<div class='magpie-view-answer-container magpie-response-multi-dropdown'>
       
            <div class= 'response-table'>
                <p class='magpie-view-question'> ${config.data[CT].question} </p >
                <div class='magpie-view-answer-container'>
                    <p>${config.data[CT].cand1}</p><span class='magpie-response-slider-option'>${config.data[CT].optionLeft}</span>
                    <input type='range' id='response1' class='magpie-response-slider' min='0' max='100' value1='50'/>
                    <span class='magpie-response-slider-option'>${config.data[CT].optionRight}</span>
                </div>
            </div>
            <div class='magpie-view-answer-container'>
                <p>${config.data[CT].cand2}</p> <span class='magpie-response-slider-option'>${config.data[CT].optionLeft}</span>
                <input type='range' id='response2' class='magpie-response-slider' min='0' max='100' value2='50'/>
                <span class='magpie-response-slider-option'>${config.data[CT].optionRight}</span>
            </div>
            <div class='magpie-view-answer-container'>
                <p>${config.data[CT].cand3}</p> <span class='magpie-response-slider-option'>${config.data[CT].optionLeft}</span>
                <input type='range' id='response3' class='magpie-response-slider' min='0' max='100' value3='50'/>
                <span class='magpie-response-slider-option'>${config.data[CT].optionRight}</span>
            </div>
            <button id="next" class='magpie-view-button magpie-nodisplay'>Weiter</button>
            <button id="fertig" class='magpie-view-button'>Fertig</button>
          </div>`;
        },
        handle_response_function: function (config, CT, magpie, answer_container_generator, startingTime) {


            $(".magpie-view").append(answer_container_generator(config, CT));
            let response_list = new Array(trial_info.multi_slider_rating_trials.length);
            let next_change = [0, 0, 0];
            response1 = $("#response1");
            response2 = $("#response2");
            response3 = $("#response3");
            // checks if the slider has been changed
            response1.on("change", function () {

                response_list[0] = (response1.val());
                next_change[0] = 1;
            });
            response2.on("change", function () {

                response_list[1] = (response2.val());
                next_change[1] = 1;
            });
            response3.on("change", function () {

                response_list[2] = (response3.val());
                next_change[2] = 1;
            });

            response1.on("click", function () {

                response_list[0] = (response1.val());
                next_change[0] = 1;
            });
            response2.on("click", function () {

                response_list[1] = (response2.val());
                next_change[1] = 1;
            });
            response3.on("click", function () {

                response_list[2] = (response3.val());
                next_change[2] = 1;
            });

            const respList = [0, 0, 0];


            $("#fertig").on("click", function () {
                // keeps the order of the results always the same alphabetically after surname Baerbock, Laschet, Scholz
                for (let i = 0; i < respList.length; i++) {
                    respList[beCand[i][1]] = response_list[i];
                    //console.log(respList)
                }
                let sum = 0;

                for (let i = 0; i < next_change.length; i++) {
                    sum += next_change[i];
                    // console.log(sum)
                }
                if (sum == next_change.length) {
                    $("#fertig").addClass("magpie-nodisplay");
                    $("#next").removeClass("magpie-nodisplay");
                }

            });


            $("#next").on("click", function () {
                const RT = Date.now() - startingTime; // measure RT before anything else
                let trial_data = {
                    trial_name: config.name,
                    trial_number: CT + 1,
                    candRating: respList,
                    RT: RT
                };

                trial_data = magpieUtils.view.save_config_trial_data(config.data[CT], trial_data);

                magpie.trial_data.push(trial_data);
                magpie.findNextView();
            });
        }
    }
);


const textbox = magpieViews.view_generator("textbox_input", {
    trials: trial_info.textbox_trials.length,
    name: 'textbox',
    data: trial_info.textbox_trials,
    buttonText: 'Weiter'
},
    {
        answer_container_generator: function (config, CT) {
            return `<p class='magpie-view-question'>${config.data[CT].question} </p>

                    <div class='magpie-view-answer-container'>
                        <textarea name='textbox-input' rows=10 cols=50 class='magpie-response-text' />
                    </div>
                    <button id='next' class='magpie-view-button magpie-nodisplay'>Weiter</button>
                    
                    `
            
        }
    },
    {
        stimulus_container_generator: function (config, CT) {
            return `<p class='magpie-view-question'>${config.data[CT].question} </p>

                    <div class='magpie-view-answer-container'>
                        <textarea name='textbox-input' rows=10 cols=50 class='magpie-response-text' />
                    </div>
                    <button id='next' class='magpie-view-button magpie-nodisplay'>Weiter</button>
                    
                    `;
        }
    },
    {handle_response_function: function(config, CT, magpie, answer_container_generator, startingTime) {
        let next;
        let textInput;
        const minChars = config.data[CT].min_chars === undefined ? 10 : config.data[CT].min_chars;

        $(".magpie-view").append(answer_container_generator(config, CT));

        next = $("#next");
        textInput = $("textarea");

        // attaches an event listener to the textbox input
        textInput.on("keyup", function() {
            // if the text is longer than (in this case) 10 characters without the spaces
            // the 'next' button appears
            if (textInput.val().trim().length > minChars) {
                next.removeClass("magpie-nodisplay");
            } else {
                next.addClass("magpie-nodisplay");
            }
        });

        // the trial data gets added to the trial object
        next.on("click", function() {
            const RT = Date.now() - startingTime; // measure RT before anything else
            let trial_data = {
                trial_name: config.name,
                trial_number: CT + 1,
                tcresponse: textInput.val().trim(),
                RT: RT
            };

            trial_data = magpieUtils.view.save_config_trial_data(config.data[CT], trial_data);

            magpie.trial_data.push(trial_data);
            magpie.findNextView();
        });
    }
}
);
const mediaConsumption = magpieViews.view_generator('sentence_choice',
    {
        trials: 1,
        name: "mediaConsumption",
        data: trial_info.mediaConsumption_trials
    },
    {
        answer_container_generator: function (config, CT) {
            return `<div class='magpie-view-answer-container'>
                    <p class='magpie-view-question'>${config.data[CT].question}</p>
                    <label for='s1' class='magpie-response-sentence'>${config.data[CT].option1}</label>
                    <input type='radio' name='answer' id='s1' value="${config.data[CT].option1}" />
                    <label for='s2' class='magpie-response-sentence'>${config.data[CT].option2}</label>
                    <input type='radio' name='answer' id='s2' value="${config.data[CT].option2}" />
                    <label for='s3' class='magpie-response-sentence'>${config.data[CT].option3}</label>
                    <input type='radio' name='answer' id='s3' value="${config.data[CT].option3}" />
                    <label for='s4' class='magpie-response-sentence'>${config.data[CT].option4}</label>
                    <input type='radio' name='answer' id='s4' value="${config.data[CT].option4}" />
                    <label for='s5' class='magpie-response-sentence'>${config.data[CT].option5}</label>
                    <input type='radio' name='answer' id='s5' value="${config.data[CT].option5}" />
                   
                </div>`
        }
    },
    {handle_response_function: function(config, CT, magpie, answer_container_generator, startingTime) {
        $(".magpie-view").append(answer_container_generator(config, CT));

        // attaches an event listener to the yes / no radio inputs
        // when an input is selected a response property with a value equal
        // to the answer is added to the trial object
        // as well as a readingTimes property with value
        $("input[name=answer]").on("change", function() {
            const RT = Date.now() - startingTime;
            let trial_data = {
                trial_name: config.name,
                trial_number: CT + 1,
                mcresponse: $("input[name=answer]:checked").val(),
                RT: RT
            };

            trial_data = magpieUtils.view.save_config_trial_data(config.data[CT], trial_data);

            magpie.trial_data.push(trial_data);
            magpie.findNextView();
        });
    }
}
);
// There are many more templates available:
// forced_choice, slider_rating, dropdown_choice, testbox_input, rating_scale, image_selection, sentence_choice,
// key_press, self_paced_reading and self_paced_reading_rating_scale
