// In this file you can specify the trial data for your experiment


const trial_info = {
    mediaConsumption_trials: [
        {
            question: "Wie h&aumlufig verfolgen Sie Neuigkeiten zur Bundestagwahl 2021 und zum laufenden Wahlkampf?",
            option1: "Einmal oder mehrmals am Tag",
            option2: "Einmal oder mehrmals in der Woche",
            option3: "Einmal oder mehrmals im Monat",
            option4: "Seltener als einmal im Monat",
            option5:"Nie" 
        }
    ],

    multi_slider_rating_trials: [
        {
            question: "Mit welcher Wahrscheinlichkeit &uumlbernimmt welche Person das Kanzleramt?",
            optionLeft:     'Sehr unwahrscheinlich',
            optionRight:    'Sehr wahrscheinlich',
            cand1: beCand[0][0],
            cand2: beCand[1][0], 
            cand3: beCand[2][0]
        }

    ],
    spr_trials: [
        {
            QUD: '&Uumlbung 1',
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: "Die L&aumlnder k&oumlnnen &uumlber den Bundesrat Einfluss nehmen.",
            option1: "Wahr",
            option2: "Falsch",
            sprCQ_expected: "Wahr",
            sentence: "Der | Bundesrat | ist | ein | Verfassungsorgan | der | Bundesrepublik | Deutschland. | &Uumlber | den | Bundesrat | wirken | die | L&aumlnder | bei | der | Gesetzgebung | und | Verwaltung | des | Bundes | sowie | in | Angelegenheiten | der | Europ&aumlischen | Union | mit. | Jedes | Land | ist | durch | Mitglieder | seiner | Landesregierung | im | Bundesrat | vertreten.",
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags empfinden. ",
            optionLeft:"Sehr unnat&uumlrlich",
            optionRight:"Sehr nat&uumlrlich"
        },
        {
            QUD: '&Uumlbung 2',
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: "Der Bundestag ist das gesetzgebende Organ der Bundesrepublik Deutschland.",
            option1: "Wahr",
            option2: "Falsch",
            sprCQ_expected: "Wahr",
            sentence: "Der | Deutsche | Bundestag | ist | das | Parlament | und | somit | das | gesetzgebende | Organ | der | Bundesrepublik | Deutschland | mit | Sitz | in | Berlin. | Der | Bundestag | wird | im | politischen | System | Deutschlands | als | einziges | Verfassungsorgan | des | Bundes | unmittelbar | vom | Staatsvolk | gew&aumlhlt. | Der | Bundestag | hat | eine | Vielzahl | von | Aufgaben.",
            sprCQ_expected: "Wahr",
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        },
        {

            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[0][1],
            option1: "Wahr",
            option2: "Falsch",            
            sentence: sprComplete[0][0],
            sprCQ_expected: sprComplete[0][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"

        },
        {
            
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[1][1],
            option1: "Wahr",
            option2: "Falsch",
            sentence: sprComplete[1][0],
            sprCQ_expected: sprComplete[1][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        },
        {
            
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[2][1],
            option1: "Wahr",
            option2: "Falsch",
            sentence: sprComplete[2][0],
            sprCQ_expected: sprComplete[2][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        },
        {
            
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[3][1],
            option1: "Wahr",
            option2: "Falsch",
            sentence: sprComplete[3][0],
            sprCQ_expected: sprComplete[3][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        },
        {
            
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[4][1],
            option1: "Wahr",
            option2: "Falsch",
            sentence: sprComplete[4][0],
            sprCQ_expected: sprComplete[4][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        },
        {
            
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[5][1],
            option1: "Wahr",
            option2: "Falsch",
            sentence: sprComplete[5][0],
            sprCQ_expected: sprComplete[5][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        },
        {
            
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[6][1],
            option1: "Wahr",
            option2: "Falsch",
            sentence: sprComplete[6][0],
            sprCQ_expected: sprComplete[6][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        },
        {
            
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[7][1],
            option1: "Wahr",
            option2: "Falsch",
            sentence: sprComplete[7][0],
            sprCQ_expected: sprComplete[8][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        },
        {
            
            help_text: 'Bitte die Leertaste dr&uumlcken.',
            question: sprComplete[8][1],
            option1: "Wahr",
            option2: "Falsch",
            sentence: sprComplete[8][0],
            sprCQ_expected: sprComplete[8][2],
            questionR: "Bitte bewerten Sie, als wie nat&uumlrlich Sie diesen Text im Sinne eines Medienbeitrags zu der anstehenden Bundestagswahl empfinden. ",
            optionLeft: "Sehr unnat&uumlrlich",
            optionRight: "Sehr nat&uumlrlich"
        }

    ],
    textbox_trials: [
        {
            QUD: "<strong>Bitte vervollst&aumlndigen Sie den Text.</strong>",
            question: "Nach der Bundestagswahl am 26. September entscheidet sich, wer f&uumlr die n&aumlchste Legislaturperiode das Kanzleramt &uumlbernimmt. Nach der geheimen Wahl durch die Mitglieder des Bundestages, wird ..."

        },
        {
            QUD: "<strong>Bitte vervollst&aumlndigen Sie den Text.</strong>",
            question: "Nach der Bundestagswahl am 26. September entscheidet sich, wer f&uumlr die n&aumlchste Legislaturperiode das Kanzleramt &uumlbernimmt. Nach der Ablegung des Amtseids vor dem Bundestag, wird ..."

        },
        {
            QUD: "<strong>Bitte vervollst&aumlndigen Sie den Text.</strong>",
            question: "Nach der Bundestagswahl am 26. September entscheidet sich, wer f&uumlr die n&aumlchste Legislaturperiode das Kanzleramt &uumlbernimmt. W&aumlhrend der ersten Rede vor dem Bundestag, wird ..."
        },
        {
            QUD: "<strong>Bitte vervollst&aumlndigen Sie den Text.</strong>",
            question: "Nach der Bundestagswahl am 26. September entscheidet sich, wer f&uumlr die n&aumlchste Legislaturperiode das Kanzleramt &uumlbernimmt. W&aumlhrend der Zeit im Kanzleramt, wird..."
        }

    ]
};
